package treebase;

import esinf_2019_2na_d_tp3.Pais;
import java.util.Stack;

public class TwoDTree extends BST<Pais> {

    protected int compareElements(Pais firstElement, Pais secondElement, int level) {
        float firstValue = (level % 2 == 0) ? firstElement.getLatitude() : firstElement.getLongitude();
        float secondValue = (level % 2 == 0) ? secondElement.getLatitude() : secondElement.getLongitude();
        if (firstValue == secondValue) {
            return 0;
        }
        return (firstValue > secondValue) ? -1 : 1;
    }

    /*
    * Inserts an element in the tree.
     */
    @Override
    public void insert(Pais element) {
        Node node = this.insert(element, this.root, 0);
        if (this.isEmpty()) {
            this.root = node;
        }
    }

    private Node<Pais> insert(Pais element, Node<Pais> node, int level) {
        if (node == null) {
            return new Node(element, null, null);
        }

        int comparison = this.compareElements(node.getElement(), element, level);
        int newLevel = level + 1;

        
        if (comparison < 0) {
            node.setLeft(insert(element, node.getLeft(), newLevel));
        } else {
            node.setRight(insert(element, node.getRight(), newLevel));
        }
        
        return node;
    }

    /**
     * Removes an element from the tree maintaining its consistency as a Binary
     * Search Tree.
     */
    @Override
    public void remove(Pais element) {
        root = remove(element, root, 0);
    }

    private Node<Pais> remove(Pais element, Node<Pais> node, int level) {

        if (node == null) {
            return null;
        }

        int newLevel = level + 1;
        int comparison = this.compareElements(element, node.getElement(), level);
        if (comparison == 0) {
            // node is the Node to be removed
            if (node.getLeft() == null && node.getRight() == null) { //node is a leaf (has no childs)
                return null;
            }
            if (node.getLeft() == null) {   //has only right child
                return node.getRight();
            }
            if (node.getRight() == null) {  //has only left child
                return node.getLeft();
            }
            Node<Pais> minNode = smallestElement(node.getRight());
            Pais min = minNode.getElement();
            node.setElement(min);
            node.setRight(remove(min, node.getRight(), newLevel));
        } else if (comparison < 0) {
            node.setLeft(remove(element, node.getLeft(), newLevel));
        } else {
            node.setRight(remove(element, node.getRight(), newLevel));
        }

        return node;
    }

    /**
     * Returns the Node containing a specific Element, or null otherwise.
     *
     * @param element the element to find
     * @return the Node that contains the Element, or null otherwise
     */
    public Pais find(Pais element) {
        Node<Pais> node = this.find(element, root, 0);
        if (node == null) {
            return null;
        }
        return node.getElement();
    }

    /**
     * Returns the Node containing a specific Element, or null otherwise.
     *
     * @param element the element to find
     * @param node the current node on the tree
     * @return the Node that contains the Element, or null otherwise
     *
     * This method despite not being essential is very useful. It is written
     * here in order to be used by this class and its subclasses avoiding
     * recoding. So its access level is protected
     */
    protected Node<Pais> find(Pais element, Node<Pais> node, int level) {
        if (node == null) {
            return null;
        }
        Pais nodeElement = node.getElement();
        int comparison = this.compareElements(nodeElement, element, level);
        int newLevel = ++level;
        if (comparison == 0) {
            return node;
        }
        if (comparison < 0) {
            return find(element, node.getLeft(), newLevel);
        }
        return find(element, node.getRight(), newLevel);
    }
    
    public Iterable<Pais> range(float latMin, float longMin, float latMax, float longMax) {
        Stack<Pais> paises = new Stack<>();
        this.recursiveRange(latMin, longMin, latMax, longMax, this.root, paises, true);
        return paises;
    }
    
    private void recursiveRange(float latMin, float longMin, float latMax, float longMax, 
            Node<Pais> node, Stack<Pais> paises, boolean evenLevel) {
        if (node == null) return;
        Pais nodeElement = node.getElement();
        float nodeLat = nodeElement.getLatitude();
        float nodeLong = nodeElement.getLongitude();
        
        if (latMin <= nodeLat && latMax >= nodeLat 
            && longMin <= nodeLong && longMax >= nodeLong) {
            paises.add(nodeElement);
        }
        
        if (evenLevel) {
            if (latMin < nodeLat) {
                this.recursiveRange(latMin, longMin, latMax, longMax, node.getLeft(), paises, !evenLevel);
            } 
            
            if (latMax > nodeLat) {
                this.recursiveRange(latMin, longMin, latMax, longMax, node.getRight(), paises, !evenLevel);
            }
        } else {
            if (longMin < nodeLong) {
                this.recursiveRange(latMin, longMin, latMax, longMax, node.getLeft(), paises, !evenLevel);
            }
            if (longMax > nodeLong) {
                this.recursiveRange(latMin, longMin, latMax, longMax, node.getRight(), paises, !evenLevel);
            }
        }
    }

    public Pais nearest(float latitude, float longitude){               
        return nearest(latitude, longitude, root, root.getElement(), true);
    }
    
    private Pais nearest(float latitude, float longitude, Node<Pais> node, Pais best, boolean evenLevel){
        
        if (node == null) return best;        
        if (node.getElement().getLatitude() == latitude 
            && node.getElement().getLongitude() == longitude) {                
            return node.getElement();
        }
        
        double currentSmallestDistance = best.distanceSquaredTo(latitude, longitude);
        double nodeDistance = node.getElement().distanceSquaredTo(latitude, longitude);
        
        if (nodeDistance < currentSmallestDistance) {
            best = node.getElement();
        }
        
        float toDivisionPlane = compareLatLong(latitude, longitude, node, evenLevel);        
        if (toDivisionPlane < 0) {
            best = nearest(latitude, longitude, node.getLeft(), best, !evenLevel);
            if (best.distanceSquaredTo(latitude, longitude) >=
                    toDivisionPlane * toDivisionPlane) {
                best = nearest(latitude, longitude, node.getRight(), best, !evenLevel);
            }
        } else {
            best = nearest(latitude, longitude, node.getRight(), best, !evenLevel);
            if (best.distanceSquaredTo(latitude, longitude) >=
                    toDivisionPlane * toDivisionPlane) {
                best = nearest(latitude, longitude, node.getLeft(), best, !evenLevel);
            }
        }
        return best;
    }
    
    private float compareLatLong(float latitude, float longitude, 
                                Node<Pais> node, boolean evenLevel){
        if (evenLevel) {
            return latitude - node.getElement().getLatitude();
        }
        return longitude - node.getElement().getLongitude();
    }
}
