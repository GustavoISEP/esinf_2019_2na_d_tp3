/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_2019_2na_d_tp3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import treebase.BST;
import treebase.TwoDTree;

public class MapaManager {
    Map<String, BST<Pais>> continentes;
    BST<Pais> paisesTree;
    TwoDTree paises2DTree;
    
    Comparator<Pais> frontPopComparator = new Comparator<Pais>() {
        @Override
        public int compare(Pais pais1, Pais pais2) {
            int pais1FrontSize = pais1.getNeighbours().size();
            int pais2FrontSize = pais2.getNeighbours().size();
            if (pais1FrontSize == pais2FrontSize) {
                float pais1Pop = pais1.getPopulacao();
                float pais2Pop = pais2.getPopulacao();
                if (pais1Pop == pais2Pop) return 0;
                return (pais1Pop < pais2Pop) ? -1 : 1;
            }
            return (pais1FrontSize > pais2FrontSize) ? -1 : 1;
        }
    };
    
    public MapaManager() {
        this.continentes = new HashMap<>();
        this.paisesTree = new BST();
        this.paises2DTree = new TwoDTree();
    }
    
    public void addPaises(List<String> listaPaises) {
        for (String pais : listaPaises) {
            this.addPais(pais);
        }
    }
    
    public void addPais(String camposPais) {
        this.addPais(Arrays.asList(camposPais.split(",")));
    }
    
    public void addPais(List<String> camposPais) {
        String nome = camposPais.get(0).trim();
        String continente = camposPais.get(1).trim();
        Float populacao = Float.parseFloat(camposPais.get(2));
        String capital = camposPais.get(3).trim();
        Float latitude = Float.parseFloat(camposPais.get(4));
        Float longitude = Float.parseFloat(camposPais.get(5));

        Pais pais = new Pais(nome, continente, populacao, capital, latitude, longitude);
        this.paisesTree.insert(pais);
        this.paises2DTree.insert(pais);
    }
    
    public Pais getPais(String paisNome) {
        Pais paisParaNome = new Pais(paisNome);
        Pais pais = this.paisesTree.find(paisParaNome);
        return pais;
    }
    
    public List<Pais> getPaises() {
        return (List) this.paisesTree.inOrder();
    }
    
    public List<Pais> getContinente(String continente) {
        BST<Pais> continenteTree = this.continentes.get(continente);
        if (continenteTree == null) {
            continenteTree = new BST(frontPopComparator);
            for (Pais pais : this.paisesTree.inOrder()) {
                if (pais.getContinente().equals(continente)) {
                    continenteTree.insert(pais);
                }
            }
            this.continentes.put(continente, continenteTree);
        }        
        return (List) continenteTree.inOrder();
    }
    
    public void addFronteiras(List<String> listaFronteiras) {
        String[] fronteira;
        for (String fronteiraLine : listaFronteiras) {
            fronteira = fronteiraLine.replaceAll("\\s+", "").split(",");
            this.addFronteira(fronteira[0], fronteira[1]);
        }
    }
    
    public void addFronteira(String pais1, String pais2) {
        this.addFronteira(this.getPais(pais1), this.getPais(pais2));
    }
    
    public void addFronteira(Pais pais1, Pais pais2) {
        // Adicionamos cada um dos países à lista de vizinhos de cada um.
        if (!pais1.isNeighbour(pais2)) pais1.addNeighbour(pais2);
        if (!pais2.isNeighbour(pais1)) pais2.addNeighbour(pais1);
    }
    
    /**
     * Retorna o número de países existentes no mapa.
     * 
     * @return int Número de países.
     */
    public int numPaises() {
        return this.getPaises().size();
    }
    
    /**
     * Retorna uma lista com todos os vizinhos de um dado país.
     * 
     * @param paisNome
     * @return 
     */
    public List<Pais> getVizinhos(String paisNome) {
        if (paisNome == null) return null;
        Pais pais = this.getPais(paisNome);
        return pais.getNeighbours();        
    }
    
    /**
     * Retorna o país correspondente às coordenadas dadas.
     * 
     * @param latitude
     * @param longitude
     * @return 
     */
    public Pais getPaisPorCoord(float latitude, float longitude) {
        Pais paisCoord = new Pais(latitude, longitude);
        return this.paises2DTree.find(paisCoord);
    }
    
    public Pais getPaisMaisProximo(float latitude, float longitude) {
        return this.paises2DTree.nearest(latitude, longitude);
    }
    
    /**
     * Pesquisa por área geográfica com complexidade linear.
     * 
     * @param latMin
     * @param longMin
     * @param latMax
     * @param longMax
     * @return 
     */
    public List<Pais> getPaisPorAreaGeoLinear(float latMin, float longMin, 
            float latMax, float longMax) {
        List<Pais> listaPaisesLinear = new ArrayList();
        float paisLat, paisLong;
        
        for (Pais pais : this.getPaises()) {
            paisLat = pais.getLatitude();
            paisLong = pais.getLongitude();
            if (latMin <= paisLat && latMax >= paisLat 
                && longMin <= paisLong && longMax >= paisLong) {
                listaPaisesLinear.add(pais);
            }
        }
        return listaPaisesLinear;
    }
    
    /**
     * Pesquisa por área geográfica com complexidade logarítmica.
     * 
     * @param latMin
     * @param longMin
     * @param latMax
     * @param longMax
     * @return 
     */
    public Iterable<Pais> getPaisPorAreaGeo(float latMin, float longMin,
            float latMax, float longMax) {
        return this.paises2DTree.range(latMin, longMin, latMax, longMax);
    }
}
