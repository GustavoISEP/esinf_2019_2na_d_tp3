/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_2019_2na_d_tp3;

/**
 *
 * @author gusta
 */
public class DistanceUtils {
    public static double distanceBetweenCountries(Pais pais1, Pais pais2) {
        return calculateFromLatLon(pais1, pais2);
    }
    
    public static double calculateFromLatLon(Pais pais1, Pais pais2) {
        float lat1 = pais1.getLatitude();
        float lon1 = pais1.getLongitude();
        float lat2 = pais2.getLatitude();
        float lon2 = pais2.getLongitude();
        
        if ((lat1 == lat2) && (lon1 == lon2)) {
                return 0;
        } else {
            double theta = lon1 - lon2;
            double dist = Math.sin(
                Math.toRadians(lat1)) 
                * Math.sin(Math.toRadians(lat2)) 
                + Math.cos(Math.toRadians(lat1)) 
                * Math.cos(Math.toRadians(lat2)) 
                * Math.cos(Math.toRadians(theta)
            );
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            return (dist);
        }
    }
}
