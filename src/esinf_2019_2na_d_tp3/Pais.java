/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_2019_2na_d_tp3;

import java.util.ArrayList;
import java.util.List;

public class Pais implements Comparable<Pais> {
    
    private String nome;
    private String continente;
    private float populacao;
    private String capital;
    private float latitude;
    private float longitude;
    private List<Pais> neighbours;
    private int cor;

    public Pais(String nome, String continente, float populacao, String capital, float latitude, float longitude) {
        this.nome = nome;
        this.continente = continente;
        this.populacao = populacao;
        this.capital = capital;
        this.latitude = latitude;
        this.longitude = longitude;
        this.cor = -1;
        this.neighbours = new ArrayList<Pais>();
    }
    
    public Pais(String nome) {
        this.nome = nome;
    }
    
    public Pais(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    public void addFronteira(Pais pais) {
        this.neighbours.add(pais);
    }
    
    public String getNome() {
        return nome;
    }

    public String getContinente() {
        return continente;
    }

    public float getPopulacao() {
        return populacao;
    }

    public String getCapital() {
        return capital;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }
    
    public int getCor() {
        return this.cor;
    }
    
    public void setCor(int cor) {
        this.cor = cor;
    }
    
    public List<Pais> getNeighbours() {
        return this.neighbours;
    }
    
    public void addNeighbour(Pais pais) {
        this.neighbours.add(pais);
    }
    
    public boolean isNeighbour(Pais pais) {
        return this.neighbours.indexOf(pais) != -1;        
    }
    
    public int getNumberNeighbours() {
        return this.neighbours.size();
    }
    
    public double distanceSquaredTo(Pais that) {
        return this.distanceSquaredTo(that.getLatitude(), that.getLatitude());        
    }
    
    public double distanceSquaredTo(float latitude, float longitude) {
        float dx = this.getLatitude() - latitude;
        float dy = this.getLongitude() - longitude;
        return dx*dx + dy*dy;
    }
    
    @Override
    public String toString() {
        return String.format(
            "%s (lat: %s; long: %s)", 
            this.nome, this.latitude, this.longitude
        );
    }
    
    @Override
    public int compareTo(Pais o) {
        return this.nome.compareTo(o.nome);        
    }
}
