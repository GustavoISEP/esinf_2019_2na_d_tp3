/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_2019_2na_d_tp3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gusta
 */
public class MapaManagerTest {

    MapaManager manager;

    public MapaManagerTest() throws IOException, Exception {
        manager = new MapaManager();
        List<String> paises = Files.lines(Paths.get("paises.txt")).collect(Collectors.toList());
        manager.addPaises(paises);

        List<String> fronteiras = Files.lines(Paths.get("fronteiras.txt")).collect(Collectors.toList());
        manager.addFronteiras(fronteiras);
    }
    
    @Test
    public void testMapaManager() {
        System.out.println("test MapaManager");
        int numPaises = 59;
        assertTrue("O mapa deve conter 59 países", manager.numPaises() == numPaises);
        
        List<Pais> listaPaises = manager.getPaises();
        String capital = "tirana";
        String capitalPrimeiroElemento = listaPaises.get(0).getCapital();
        assertTrue(
            "A capital do primeiro elemento da árvore deve ser Tirana", 
            capital.equals(capitalPrimeiroElemento)
        );
        
        String ultimaCapital = "caracas";
        String capitalUltimoElemento = listaPaises.get(numPaises - 1).getCapital();
        assertTrue(
            "A capital do último elemento deve ser Caracas",
            ultimaCapital.equals(capitalUltimoElemento)
        );
    }
    
    @Test
    public void testBST() {
        System.out.println("Test BST");
        
    }
    
    @Test
    public void testGetVizinhos() {
        List<Pais> paisesVizinhos = manager.getVizinhos("espanha");
        assertTrue("A Espanha deve ter 2 vizinhos", paisesVizinhos.size() == 2);
        
    }
    
    @Test
    public void testGetContinente() {
        List<Pais> paisesVizinhos = manager.getContinente("americasul");
        assertTrue("", true);
    }

    @Test
    public void testFind() {
        System.out.println("Test exact find");
        String capital = "lapaz";
        Pais pais = manager.getPaisPorCoord(-16.5000000f, -68.1500000f);
        assertTrue(
                "A capital retornada deve ser Lapaz",
                pais.getCapital().equals(capital)
        );

        capital = "helsinque";
        pais = manager.getPaisPorCoord(60.1698791f, 24.9384078f);
        assertTrue(
                "A capital retornada deve ser Helsínquia",
                pais.getCapital().equals(capital)
        );

        pais = manager.getPaisPorCoord(49.8566667f, 3.3509871f);
        assertTrue(
                "Não deve retornar resultado",
                pais == null
        );
    }

    @Test
    public void testNN() {
        System.out.println("Test nearest neighbour");
        Pais pais = manager.getPaisMaisProximo(49.81f, 6.12f);
        assertTrue(
            "O pais retornado deve ser Luxemburgo",
            pais.getNome().equals("luxemburgo")
        );
        
        pais = manager.getPaisMaisProximo(39.249f, -2.181f);
        assertTrue(
            "O pais retornado deve ser Espanha",
            pais.getNome().equals("espanha")
        );
        
        pais = manager.getPaisMaisProximo(-15.432f, -49.973f);
        assertTrue(
            "O pais retornado deve ser Brasil",
            pais.getNome().equals("brasil")
        );
        
        pais = manager.getPaisMaisProximo(90f, 0f);
        assertTrue(
            "O pais retornado deve ser Noruega",
            pais.getNome().equals("noruega")
        );
    }

    @Test
    public void testGeoSearch() {
        System.out.println("Test range search");
        float latMin = 24f, longMin = -10f, latMax = 50.0f, longMax = 30.0f;
        List<Pais> listaPaisesInRect = manager.getPaisPorAreaGeoLinear(latMin, longMin, latMax, longMax);
        List<Pais> listaPaises = (List) manager.getPaisPorAreaGeo(latMin, longMin, latMax, longMax);
        assertTrue(
            "O número de países dentro do rectângulo tem de ser " + listaPaisesInRect.size(),
            listaPaisesInRect.size() == listaPaises.size()
        );
        
        // Bulgaria: 42.6976246, 23.3222924
        latMin = 42f;
        longMin = 23f;
        latMax = 43.0f;
        longMax = 23.5f;
        listaPaisesInRect = manager.getPaisPorAreaGeoLinear(latMin, longMin, latMax, longMax);
        listaPaises = (List) manager.getPaisPorAreaGeo(latMin, longMin, latMax, longMax);
        assertTrue(
            "O número de países dentro do rectângulo tem de ser " + listaPaisesInRect.size(),
            listaPaisesInRect.size() == listaPaises.size()
        );
        assertTrue(
            "Apenas a Bulgaria deve constar da lista",
            listaPaises.get(0).getNome().equals("bulgaria")
        );
        
        latMin = -90f;
        longMin = -120f;
        latMax = 90f;
        longMax = 120f;
        listaPaises = (List) manager.getPaisPorAreaGeo(latMin, longMin, latMax, longMax);
        assertTrue(
            "Todos os países têm de estar dentro da área." + listaPaisesInRect.size(),
            manager.getPaises().size() == listaPaises.size()
        );

        latMin = 0f;
        longMin = 0f;
        latMax = 0f;
        longMax = 0f;
        listaPaises = (List) manager.getPaisPorAreaGeo(latMin, longMin, latMax, longMax);
        assertTrue(
            "Nenhum país pode estar dentro da área." + listaPaisesInRect.size(),
            listaPaises.size() == 0
        );  
    }
    
}
